﻿using UnityEngine;
using System.Linq;

namespace ChronosFramework
{
    public class CoreManager : Singleton<CoreManager>
    {
        public EasingCurve[] easingCurves;

        public event UpdateAction OnUpdate;
        public event UpdateAction OnFixedUpdate;
        
        void Update()
        {
            if (OnUpdate != null)
            {
                OnUpdate();
            }
        }

        void FixedUpdate()
        {
            if (OnFixedUpdate != null)
            {
                OnFixedUpdate();
            }
        }

        public AnimationCurve GetCurve(EasingType easing)
        {
            return easingCurves.First(result => result.easingType == easing).curve;
        }
    }
}