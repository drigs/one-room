﻿using UnityEngine;
using System.Collections;

namespace ChronosFramework
{
    public class SplineWalker : MonoBehaviour
    {
        public Spline spline;
        public float duration;
        public bool looksForward;
        public bool loop;
        public bool reverse;
        public bool startOnAwake;

        Tween splineTween;

        void Start()
        {
            if (startOnAwake)
            {
                StartWalking();
            }
        }

        public void StartWalking()
        {
            splineTween = new Tween(transform, spline, duration, true, looksForward, reverse);
            splineTween.isLoop = loop;
        }

        public void StopWalking()
        {
            if (splineTween != null)
            {
                splineTween.Stop();
            }
        }
    }
}