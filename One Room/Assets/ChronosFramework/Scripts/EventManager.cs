﻿namespace ChronosFramework
{
    static class EventManager
    {
        public static void AddToUpdate(this UpdateAction listener)
        {
            CoreManager.Instance.OnUpdate += listener;
        }

        public static void AddToFixedUpdate(this UpdateAction listener)
        {
            CoreManager.Instance.OnFixedUpdate += listener;
        }

        public static void RemoveFromUpdate(this UpdateAction listener)
        {
            CoreManager.Instance.OnUpdate -= listener;
        }

        public static void RemoveFromFixedUpdate(this UpdateAction listener)
        {
            CoreManager.Instance.OnFixedUpdate -= listener;
        }
    }
}
