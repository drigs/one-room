﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;

namespace ChronosFramework
{
    [InitializeOnLoad]
    public class ChronosLoader : MonoBehaviour
    {
        static CoreManager coreManager;

        static ChronosLoader()
        {
            EditorApplication.CallbackFunction Load = null; Load = delegate
            {
                coreManager = FindObjectOfType<CoreManager>();

                if (coreManager)
                {
                    coreManager.gameObject.SetActive(true);
                    coreManager.enabled = true;
                }
                else
                {
                    coreManager = Resources.Load<CoreManager>("CoreManager");
                    coreManager = PrefabUtility.InstantiatePrefab(coreManager) as CoreManager;
                    coreManager.gameObject.name = "CoreManager";
                }
            };

            EditorApplication.playmodeStateChanged += Load;
        }
    }
}
#endif
