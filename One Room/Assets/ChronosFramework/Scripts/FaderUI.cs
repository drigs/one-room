﻿using UnityEngine;
using UnityEngine.UI;

namespace ChronosFramework
{
    public enum FadeType
    {
        FadeIn, FadeOut, FadeInOut
    }

    public class FaderUI : MonoBehaviour
    {
        public Graphic target;
        public FadeType fadeType;
        public float delay;
        public float duration = 1;
        public bool beginOnStart = true;

        Tween fadeTween;

        void Start()
        {
            if (beginOnStart)
            {
                StartFade();
            }
        }

        public void StartFade()
        {
            if (delay > 0)
            {
                Timer delayTimer = new Timer(duration, Fade);
            }
            else
            {
                Fade();
            }
        }

        void Fade()
        {
            switch (fadeType)
            {
                case FadeType.FadeIn:
                    fadeTween = target.FadeIn(duration);
                    break;
                case FadeType.FadeOut:
                    fadeTween = target.FadeOut(duration);
                    break;
                case FadeType.FadeInOut:
                    fadeTween = target.FadeIn(duration / 2f);
                    fadeTween.OnComplete += () =>
                    {
                        fadeTween = target.FadeOut(duration / 2f);
                    };
                    break;
                default:
                    break;
            }
        }
    }
}
