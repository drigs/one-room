﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GuessObject : MonoBehaviour
{
	public string groupName;
    public string[] options;
    public int chosenOptionIndex;
    public string ChosenOption { get { return options[chosenOptionIndex]; } }
}
