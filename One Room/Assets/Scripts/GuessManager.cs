﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class GuessManager : Singleton<GuessManager>
{
    public string[] solution;

    List<GuessObject> guessObjects = new List<GuessObject>();

    UIGuessNotes guessNotes;

    void Start()
    {
        guessObjects.AddRange(FindObjectsOfType<GuessObject>());
        guessNotes = FindObjectOfType<UIGuessNotes>();
    }

#if UNITY_EDITOR
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log(GetSolution() ? "Right answer" : "Wrong answer");
        }

        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log(ShowGuesses());
        }

        if (Input.GetKeyDown(KeyCode.Backspace))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene("MainMenu");
        }
    }
#endif
        
    public bool GetSolution()
    {
        return guessObjects.All((g) =>
        {
            return solution.Contains(g.groupName + ": " + g.ChosenOption) || 
            (g.ChosenOption == "None" && guessNotes.guessListCount == solution.Length);
        });
    }

    public void CheckSolution()
    {
        Debug.Log(GetSolution() ? "Right answer" : "Wrong answer");
    }

    public string ShowGuesses()
    {
        string playerGuesses = "";
        guessObjects.ForEach((g) => playerGuesses += g.ChosenOption + " ");

        return playerGuesses;
    }
}
