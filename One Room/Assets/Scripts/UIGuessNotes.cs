﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using ChronosFramework;

public class UIGuessNotes : MonoBehaviour {

	public GameObject listGuessPrefab;
	public GameObject guessListPanelPrefab;
    public GameObject rightAnswerPrefab;
    public GameObject wrongAnswerPrefab;

    public RectTransform rectTransform;
	public Button openCloseButton;
	public Text openCloseText;

	public GuessObject[] guessObjects;

    public AudioSource audioSource { get; set; }
    public AudioClip writeSound;
    public AudioClip eraseSound;
    public AudioClip notebookSound;
    public AudioClip failSound;
    public AudioClip successSound;

    public int guessListCount;

    private Transform guessListPanel;
    private bool isOpen = false;

	private void Awake()
	{
		guessObjects = FindObjectsOfType<GuessObject>();
        audioSource = GetComponent<AudioSource>();
	}

	public void OpenClose()
	{
        audioSource.PlayOneShot(notebookSound);

		if (isOpen == false) {
			Open ();
		} else {
			Close ();
		}
	}

	private void Open()
	{
		isOpen = true;
		openCloseButton.interactable = false;

		Vector3 initialPosition = new Vector2(-10, 30);
		Vector3 finalPosition = new Vector2(-10, 600);

		Tween openTween = new Tween (initialPosition, finalPosition, 0.75f, false, EasingType.EaseOut, delegate(Vector3 interpolatedValue)
		{
			rectTransform.anchoredPosition = interpolatedValue;
		});
		openTween.OnComplete += delegate {
			openCloseButton.interactable = true;
			openCloseText.text = "Close Guess List";
		};
	}

	private void Close()
	{
		isOpen = false;
		openCloseButton.interactable = false;

		Vector3 initialPosition = new Vector2(-10, 600);
		Vector3 finalPosition = new Vector2(-10, 30);

		Tween openTween = new Tween (initialPosition, finalPosition, 0.75f, false, EasingType.EaseOut, delegate(Vector3 interpolatedValue)
		{
			rectTransform.anchoredPosition = interpolatedValue;
		});
		openTween.OnComplete += delegate {
			openCloseButton.interactable = true;
			openCloseText.text = "Open Guess List";
		};
	}

	public void UpdateList()
	{
		if (guessListPanel != null) {
			Destroy (guessListPanel.gameObject);
		}

		guessListPanel = Instantiate (guessListPanelPrefab).transform;
		guessListPanel.SetParent (transform);
		guessListPanel.GetComponent<RectTransform>().localScale = Vector2.one;
		guessListPanel.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, -30);

		guessListCount = 0;

		for (int i = 0; i < guessObjects.Length; i++)
		{
			if (guessObjects [i].ChosenOption == "None")
				continue;

			UIGuessNote newListGuessNote = Instantiate (listGuessPrefab).GetComponent<UIGuessNote> ();

			newListGuessNote.rectTransform.SetParent (guessListPanel);
			newListGuessNote.rectTransform.localScale = Vector2.one;
			newListGuessNote.rectTransform.anchoredPosition = new Vector2 (0, (guessListCount * -50) -10);
			newListGuessNote.text.text = guessObjects[i].groupName + ": " + guessObjects[i].ChosenOption;

			guessListCount++;

			newListGuessNote.guessObjectIndex = i;
			newListGuessNote.onClick = OnNoteClick;
		}
        Debug.Log("Guess List Count: " + guessListCount);
	}

	private void OnNoteClick(int guessObjectIndex)
	{
        audioSource.PlayOneShot(eraseSound, 1.5f);

		guessObjects[guessObjectIndex].chosenOptionIndex = 0;

		UpdateList ();
	}

    public void CheckSolution()
    {
        if(GuessManager.Instance.GetSolution() == true)
        {
            GetComponent<CanvasGroup>().interactable = false;

            RectTransform rightAnswerPanel = Instantiate(rightAnswerPrefab).GetComponent<RectTransform>();

            rightAnswerPanel.SetParent(transform);
            rightAnswerPanel.localScale = Vector2.one;
            rightAnswerPanel.anchoredPosition = Vector2.zero;

            Timer t = new Timer(1, () => SceneManager.LoadScene("MainMenu"));
            audioSource.PlayOneShot(successSound);
        }
        else
        {
            RectTransform wrongAnswerPanel = Instantiate(wrongAnswerPrefab).GetComponent<RectTransform>();

            audioSource.PlayOneShot(failSound);

            wrongAnswerPanel.SetParent(transform);
            wrongAnswerPanel.localScale = Vector2.one;
            wrongAnswerPanel.anchoredPosition = Vector2.zero;
        }
    }
}
