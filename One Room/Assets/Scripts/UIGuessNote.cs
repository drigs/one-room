﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGuessNote : MonoBehaviour
{
	public Action<int> onClick;
	public RectTransform rectTransform;
	public Text text;
	public Button button;
	public int guessObjectIndex;

	public void OnClick()
	{
		if (onClick != null) {
			onClick(guessObjectIndex);
		}
	}
}
