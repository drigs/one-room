﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ChronosFramework;

public class SolutionFeedback : MonoBehaviour {

    private CanvasGroup canvasGroup;

	// Use this for initialization
	void Start () {
        canvasGroup = GetComponent<CanvasGroup>();

        Tween fadeOut = new Tween(1, 0, 1, false, EasingType.Linear, delegate (float interpolatedValue)
        {
            canvasGroup.alpha = interpolatedValue;
        });
        fadeOut.OnComplete += delegate
        {
            Destroy(gameObject);
        };
	}

}
