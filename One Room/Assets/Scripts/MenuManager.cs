﻿using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuManager : MonoBehaviour {

	public void LoadLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }
}
