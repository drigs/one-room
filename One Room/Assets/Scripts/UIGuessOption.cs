﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIGuessOption : MonoBehaviour
{
	public Action<int> onChoseOption;
	public CanvasGroup canvasGroup;
	public Button button;
	public Text text;
	public int index;

	public void ChooseOption()
	{
		if (onChoseOption != null) {
			onChoseOption (index);
		}
	}
}
