﻿using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ChronosFramework;

[System.Serializable]
public struct CameraTarget
{
    public Transform target;
    public CanvasGroup targetPanel;
}

public class SplineCamera : Singleton<SplineCamera>
{
    public Spline spline;
    public Transform lookTarget;
    public CameraTarget[] cameraTargets;
    AudioSource audioSource;
    float startPoint;
    float lastPoint;
    float walkDistance;
    int targetIndex;
    bool tweening;

	public Button nextButton;
	public Button previousButton;

    public void Start()
    {
        audioSource = GetComponent<AudioSource>();

        targetIndex = cameraTargets.Length - 1;
        walkDistance = 1f / spline.CurveCount;
        lookTarget.position = cameraTargets[cameraTargets.Length - 1].target.position;
        transform.position = spline.GetPoint(0);
		transform.LookAt(cameraTargets[cameraTargets.Length - 1].target.position);
        cameraTargets.ForEach((t) =>
        {
            t.target.GetComponent<Renderer>().enabled = false;
            t.targetPanel.alpha = 0;
            t.targetPanel.gameObject.SetActive(false);
        });

        OpenCanvasGroup(cameraTargets[targetIndex].targetPanel, .5f);
    }

	#if UNITY_EDITOR
    public void Update()
    {
        if (!tweening)
        {
            float axis = Input.GetAxis("Horizontal");
            if (axis != 0)
            {
                bool right = axis > 0;
                WalkSpline(right);
            }
        }
    }
	#endif

	public void MovePrevious()
	{
		WalkSpline (false);
	}

	public void MoveNext()
	{
		WalkSpline (true);
	}

    public void WalkSpline(bool reversed)
    {
		nextButton.interactable = false;
		previousButton.interactable = false;

        if (reversed)
        {
            if (startPoint == 0)
            {
                startPoint = 1;
            }
        }
        else
        {
            if (startPoint == 1)
            {
                startPoint = 0;
            }
        }

        audioSource.pitch = Random.Range(1f, 1.30f);
        audioSource.Play();

        tweening = true;

        float direction = reversed ? -walkDistance : walkDistance;
        float finalPoint = startPoint + direction;

        CloseCanvasGroups(.5f);

        int index = (int)(cameraTargets.Length * finalPoint) - 1; // Gets target index based on spline position
        targetIndex = index >= 0 ? index : cameraTargets.Length - 1; //Works best with 3?!?
    
        
        var initialRot = transform.rotation;

        var finalRot = Quaternion.LookRotation(cameraTargets[targetIndex].target.position - spline.GetPoint(finalPoint));
        
        Tween rotate = new Tween(transform.rotation, finalRot, 1f, true, EasingType.Linear, (rot) =>
        {
            transform.rotation = rot;
        });

        //Tween targetMove = new Tween(lookTarget.position, targets[targetIndex].position, 1f, true, EasingType.Linear, (pos) =>
        //{
        //    lookTarget.position = pos;
        //    transform.LookAt(lookTarget);
        //});     

        Tween splineTween = null;
        splineTween = new Tween (0, direction, 1f, true, EasingType.Linear, (point) =>
        {
            transform.position = spline.GetPoint(startPoint + point);
        });
        
        splineTween.OnComplete += delegate
        {
            startPoint = finalPoint;
            tweening = false;

			nextButton.interactable = true;
			previousButton.interactable = true;

            OpenCanvasGroup(cameraTargets[targetIndex].targetPanel, .5f);
        };
    }
    
    void OpenCanvasGroup(CanvasGroup group, float time)
    {
        group.gameObject.SetActive(true);

        Tween fade = new Tween(group.alpha, 1f, time, false, EasingType.Linear, (alpha) =>
        {
            group.alpha = alpha;
        });
    }

    void CloseCanvasGroups(float time)
    {
        cameraTargets.ForEach((t) =>
        {
            var group = t.targetPanel;

            group.GetComponentsInChildren<UIGuessGroup>().ForEach((g) => g.HideOptions());

            Tween fade = new Tween(group.alpha, 0, time, false, EasingType.Linear, (alpha) =>
            {
                group.alpha = alpha;
            });

            fade.OnComplete += delegate
            {
                group.gameObject.SetActive(false);
            };

        });
    }
}
