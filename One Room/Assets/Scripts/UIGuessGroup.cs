﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ChronosFramework;

public class UIGuessGroup : MonoBehaviour
{
	private GuessObject guessObject;

	public GameObject uiGuessOption;
	public Button showHideButton;
	public Text nameText;

	public Vector2 offset;

	private List<GameObject> uiOptionList = new List<GameObject>();

    private UIGuessNotes guessNotes;

	private bool isOpen = false;

	private void Start()
	{
        guessNotes = FindObjectOfType<UIGuessNotes>();

		guessObject = GetComponent<GuessObject>();

		CreateOptions();
	}

	private void CreateOptions()
	{
		nameText.text = guessObject.groupName;

		for (int i = 0; i < guessObject.options.Length; i++)
		{
			GameObject newUIOption = Instantiate(uiGuessOption);

			newUIOption.transform.SetParent (transform);
			newUIOption.transform.localScale = Vector3.one;
			newUIOption.SetActive(false);

			UIGuessOption newUIGuessOption = newUIOption.GetComponent<UIGuessOption> ();
			newUIGuessOption.index = i;
			newUIGuessOption.text.text = guessObject.options [i];
			newUIGuessOption.button.onClick.RemoveListener(HideOptions);
			newUIGuessOption.button.onClick.AddListener(HideOptions);
			newUIGuessOption.onChoseOption += ChooseOption;

			RectTransform newUIOptionRectTransform = newUIOption.GetComponent<RectTransform> ();
			newUIOptionRectTransform.anchoredPosition = Vector2.zero;

			uiOptionList.Add (newUIOption);
		}
	}

	public void ShowOptions()
	{
		isOpen = true;
		showHideButton.interactable = false;

		for (int i = 0; i < uiOptionList.Count; i++)
		{
			uiOptionList [i].SetActive (true);

			RectTransform uiOptionRectTransform = uiOptionList [i].GetComponent<RectTransform> ();
			UIGuessOption uiOption = uiOptionList [i].GetComponent<UIGuessOption> ();

			uiOptionRectTransform.anchoredPosition = Vector2.zero;

			Vector3 finalPosition = Vector3.zero;

			switch (i) {
			case 0: finalPosition.y = -offset.y; break;
			case 1: finalPosition.x = offset.x; break;
			case 2: finalPosition.x = -offset.x; break;
			case 3: finalPosition.y = offset.y; break;
			}

			Tween expandTween = new Tween (Vector3.zero, finalPosition, 0.65f, false, EasingType.EaseOut, delegate(Vector3 interpolatedValue) {
				uiOptionRectTransform.anchoredPosition = interpolatedValue;
			});
			
			uiOption.canvasGroup.interactable = false;

			Tween fadeInTween = new Tween (0, 1, 0.5f, false, EasingType.Linear, delegate(float interpolatedValue) {
				uiOption.canvasGroup.alpha = interpolatedValue;
			});

			fadeInTween.OnComplete += delegate
			{
				uiOption.canvasGroup.interactable = true;
			};
		}
	}

	public void HideOptions()
	{
		for (int i = 0; i < uiOptionList.Count; i++)
		{
			GameObject currentOption = uiOptionList [i];

			RectTransform uiOptionRectTransform = currentOption.GetComponent<RectTransform> ();
			CanvasGroup uiOptionCanvasGroup = currentOption.GetComponent<CanvasGroup> ();

			uiOptionRectTransform.anchoredPosition = Vector2.zero;

			Vector3 finalPosition = Vector3.zero;

			switch (i) {
			case 0: finalPosition.y = -offset.y; break;
			case 1: finalPosition.x = offset.x; break;
			case 2: finalPosition.x = -offset.x; break;
			case 3: finalPosition.y = offset.y; break;
			}

			Tween expandTween = new Tween (finalPosition, Vector3.zero, 0.65f, false, EasingType.EaseOut, delegate(Vector3 interpolatedValue) {
				uiOptionRectTransform.anchoredPosition = interpolatedValue;
			});

			uiOptionCanvasGroup.interactable = false;
			Tween fadeInTween = new Tween (1, 0, 0.5f, false, EasingType.Linear, delegate(float interpolatedValue) {
				uiOptionCanvasGroup.alpha = interpolatedValue;
			});

			fadeInTween.OnComplete += delegate {
				currentOption.SetActive (false);
				showHideButton.interactable = true;
			};
		}
	}

	private void ChooseOption(int index)
	{
        guessNotes.audioSource.PlayOneShot(guessNotes.writeSound);

		guessObject.chosenOptionIndex = index;

		guessNotes.UpdateList ();
	}
}
